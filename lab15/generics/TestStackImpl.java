package generics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestStackImpl {

	public static void main(String[] args) {
		
		List<Integer> lst= new LinkedList<>();
		lst.add(5);
		lst.add(6);
		List<String> lstStr = new LinkedList<>();
		lstStr.add("A");
		
		lstStr.add("B");
		
		List<Object> lstObj = new ArrayList<>();
		lstObj.addAll(lstStr);
		lstObj.addAll(lst);
		
		Stack<String> stack = new StackImpl<>();
		
		//stackObj.addAll(stack);
		
		
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		System.out.println(stack.toList());
		
		Stack<String> stack2 = new StackImpl<>();
		stack2.push("x");
		stack2.push("Y");
		stack.addAll(stack2);
		System.out.println(stack.toList());
		while (!stack.empty()){
			String str=stack.pop();
			System.out.println(stack.pop());
			System.out.println(str);
			}
		Stack<Integer> stackInt = new StackArrayImpl();
		stackInt.push(5);
		int a= stackInt.pop();
		Integer b;
		int c;
		c=8;
		b=c;
		
	}

}
